package ai.maum.shinhan_video_processing_server.util.logger

import ai.maum.shinhan_video_processing_server.AllOpen
import org.slf4j.Logger

@AllOpen
class DslLogger(
    protected val logger: Logger
) {
    @SuppressWarnings
    infix fun trace(message: String) {
        logger.trace(message)
    }

    @SuppressWarnings
    infix fun debug(message: String) {
        logger.debug(message)
    }

    @SuppressWarnings
    infix fun info(message: String) {
        logger.info(message)
    }

    @SuppressWarnings
    infix fun warn(message: String) {
        logger.warn(message)
    }

    @SuppressWarnings
    infix fun error(message: String) {
        logger.error(message)
    }
}
