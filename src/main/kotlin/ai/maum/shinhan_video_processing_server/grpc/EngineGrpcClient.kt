package ai.maum.shinhan_video_processing_server.grpc

import ai.maum.shinhan_video_processing_server.http.util.alias.CreateVideoOption.Companion.CHANNEL_TERMINATE_AWAIT_TIME
import ai.maum.shinhan_video_processing_server.http.util.alias.CreateVideoOption.Companion.CREATE_VIDEO_BUFFER_SIZE
import ai.maum.shinhan_video_processing_server.http.v1.caching.video.dto.VideoSynthesizeFromCachingRequest
import ai.maum.shinhan_video_processing_server.http.v1.caching.video.dto.VideoSynthesizeReportToCachingRequest
import ai.maum.shinhan_video_processing_server.util.logger.DslLogger
import com.google.protobuf.ByteString
import com.google.protobuf.Empty
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import maum.brain.tts.NgTtsServiceGrpc
import maum.brain.tts.TtsMediaResponse
import maum.brain.tts.TtsRequest
import maum.brain.wav2lip.BrainWav2Lip.VideoResolution
import maum.brain.wav2lip.BrainWav2Lip.Wav2LipInput
import maum.brain.wav2lip.BrainWav2Lip.Wav2LipResult
import maum.brain.wav2lip.Wav2LipGenerationGrpc
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxSink
import reactor.core.publisher.Mono
import reactor.core.publisher.MonoSink
import java.io.ByteArrayInputStream
import java.io.File
import java.util.concurrent.TimeUnit

@Service
class EngineGrpcClient(
    @Value("\${ai.maum.shinhan.video_processing_server.directory.root}")
    private val videoProcessingRootDirectory: String,
    @Value("\${ai.maum.shinhan.video_processing_server.location.video}")
    private val generatedVideoLocation: String
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    fun async(req: VideoSynthesizeFromCachingRequest) = Mono.create<VideoSynthesizeReportToCachingRequest> { sink ->
        control(req, asyncSink = sink)
    }

    fun stream(req: VideoSynthesizeFromCachingRequest) = Flux.create<ByteArray> { sink ->
        control(req, streamSink = sink)
    }

    private fun checkGesture(w2lChannel: ManagedChannel, gesture: String) = Wav2LipGenerationGrpc
        .newBlockingStub(w2lChannel).getAvatarInfo(Empty.getDefaultInstance())
        .videoListList.any { list -> list.action == gesture }

    private fun getRequestObserver(
        req: VideoSynthesizeFromCachingRequest,
        w2lChannel: ManagedChannel,
        asyncSink: MonoSink<VideoSynthesizeReportToCachingRequest>? = null,
        streamSink: FluxSink<ByteArray>? = null
    ) = Wav2LipGenerationGrpc.newStub(w2lChannel).determinate(object : StreamObserver<Wav2LipResult> {
        var bytes = ByteString.EMPTY

        override fun onNext(value: Wav2LipResult?) {
            value?.let { result ->
                asyncSink?.let { bytes = bytes.concat(result.video) }
                streamSink?.next(result.video.toByteArray())
            }
        }

        override fun onError(t: Throwable?) {
            log error("$t")
        }

        override fun onCompleted() {
            log info "w2l 완료: ${req.name}"
            w2lChannel.shutdown()

            if (!w2lChannel.awaitTermination(1, TimeUnit.SECONDS))
                w2lChannel.shutdownNow()

            asyncSink?.let {
                File("$videoProcessingRootDirectory$generatedVideoLocation/${req.name}")
                    .writeBytes(bytes.toByteArray())
                it.success(
                    VideoSynthesizeReportToCachingRequest(
                        req.name,
                        req.liveYn
                    )
                )
            }

            streamSink?.complete()
        }
    })

    private fun getResponseObserver(
        req: VideoSynthesizeFromCachingRequest,
        ttsChannel: ManagedChannel,
        reqObserver: StreamObserver<Wav2LipInput>
    ) = object : StreamObserver<TtsMediaResponse> {
        override fun onNext(value: TtsMediaResponse?) {
            value?.mediaData?.let {
                reqObserver.onNext(
                    Wav2LipInput.newBuilder()
                        .setAudio(it)
                        .build()
                )
            }
        }

        override fun onError(t: Throwable?) {
            log error("$t")
        }

        override fun onCompleted() {
            log info "tts 완료: ${req.name}"
            ttsChannel.shutdown()

            if (!ttsChannel.awaitTermination(CHANNEL_TERMINATE_AWAIT_TIME, TimeUnit.SECONDS))
                ttsChannel.shutdownNow()

            val buffer = ByteArray(CREATE_VIDEO_BUFFER_SIZE)
            val bis = ByteArrayInputStream(
                File(req.background.path).readBytes()
            )
            var len: Int

            while (bis.read(buffer, 0, buffer.size).also { len = it } > 0) {
                reqObserver.onNext(
                    Wav2LipInput.newBuilder()
                        .setBackground(ByteString.copyFrom(buffer, 0, len))
                        .build()
                )
            }

            reqObserver.onNext(
                Wav2LipInput.newBuilder()
                    .setResolution(
                        VideoResolution.newBuilder()
                            .setWidth(req.resolution.width)
                            .setHeight(req.resolution.height)
                            .build()
                    )
                    .build()
            )

            reqObserver.onNext(
                Wav2LipInput.newBuilder()
                    .setAction(req.gesture)
                    .build()
            )

            reqObserver.onCompleted()
            log info "w2l 요청: ${req.name}"
        }
    }

    private fun getChannel(req: VideoSynthesizeFromCachingRequest, isWav2Lip: Boolean) = when (isWav2Lip) {
        true ->
            ManagedChannelBuilder
                .forAddress(req.engine.w2lHost + req.engine.w2lPath, req.engine.w2lPort)
                .usePlaintext()
                .build()
        false ->
            ManagedChannelBuilder
                .forAddress(req.engine.ttsHost + req.engine.ttsPath, req.engine.ttsPort)
                .usePlaintext()
                .build()
    }

    private fun control(
        req: VideoSynthesizeFromCachingRequest,
        asyncSink: MonoSink<VideoSynthesizeReportToCachingRequest>? = null,
        streamSink: FluxSink<ByteArray>? = null
    ) {
        val ttsChannel = getChannel(req, false)
        val w2lChannel = getChannel(req, true)

        if (checkGesture(w2lChannel, req.gesture)) {
            log info "생성할 영상(name: ${req.name}, gesture: ${req.gesture})"
            NgTtsServiceGrpc.newStub(ttsChannel).speakWav(
                TtsRequest.newBuilder()
                    .setText(req.utter)
                    .build(),
                getResponseObserver(req, ttsChannel, getRequestObserver(req, w2lChannel, asyncSink, streamSink))
            )
        }
    }
}
