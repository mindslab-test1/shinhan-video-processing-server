package ai.maum.shinhan_video_processing_server.http.util.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.WebClient

@Configuration
class WebClientConfig(
    @Value("\${ai.maum.shinhan.caching_server.address}")
    private val cachingServerUrl: String
) {
    @Bean
    fun cachingServerWebClient() = WebClient.create(cachingServerUrl)
}
