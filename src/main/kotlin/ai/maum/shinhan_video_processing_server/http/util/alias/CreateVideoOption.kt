package ai.maum.shinhan_video_processing_server.http.util.alias

class CreateVideoOption private constructor() {
    companion object {
        const val CHANNEL_TERMINATE_AWAIT_TIME = 1L
        const val CREATE_VIDEO_BUFFER_SIZE = 1024 * 16
    }
}
