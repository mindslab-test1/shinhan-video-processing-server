package ai.maum.shinhan_video_processing_server.http.util.alias

import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RequestPredicates

class ContentType private constructor() {
    companion object {
        val REQUEST_JSON = RequestPredicates.contentType(MediaType.APPLICATION_JSON)
        val REQUEST_MULTIPART = RequestPredicates.contentType(MediaType.MULTIPART_FORM_DATA)
        val RESPONSE_JSON = RequestPredicates.accept(MediaType.APPLICATION_JSON)
        val RESPONSE_OCTET = RequestPredicates.accept(MediaType.APPLICATION_OCTET_STREAM)
    }
}
