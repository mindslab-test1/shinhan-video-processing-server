package ai.maum.shinhan_video_processing_server.http.v1.caching.video

import ai.maum.shinhan_video_processing_server.grpc.EngineGrpcClient
import ai.maum.shinhan_video_processing_server.http.v1.caching.video.dto.VideoSynthesizeFromCachingRequest
import ai.maum.shinhan_video_processing_server.util.logger.DslLogger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.body
import org.springframework.web.reactive.function.client.bodyToMono
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.body
import org.springframework.web.reactive.function.server.bodyToMono

@Component
class SynthesizeHandler(
    private val grpcClient: EngineGrpcClient,
    private val cachingServerWebClient: WebClient
) {
    private val log = DslLogger(LoggerFactory.getLogger(this::class.java))

    fun synthesizeVideo(request: ServerRequest) = request.bodyToMono<VideoSynthesizeFromCachingRequest>()
        .flatMap { req ->
            log info "Caching으로부터 생성 요청된 영상: $req"

            cachingServerWebClient
                .post()
                .uri("/caching/v1/vp/report/create")
                .contentType(MediaType.APPLICATION_JSON)
                .body(grpcClient.async(req))
                .retrieve()
                .bodyToMono<Void>()
                .subscribe()

            ServerResponse.ok().build()
        }

    fun synthesizeVideoStreaming(request: ServerRequest) = request.bodyToMono<VideoSynthesizeFromCachingRequest>()
        .flatMap { req ->
            ServerResponse.ok()
                .body(grpcClient.stream(req))
        }
}
