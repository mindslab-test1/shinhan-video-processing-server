package ai.maum.shinhan_video_processing_server.http.v1.caching

import ai.maum.shinhan_video_processing_server.http.util.alias.ContentType.Companion.REQUEST_JSON
import ai.maum.shinhan_video_processing_server.http.util.alias.ContentType.Companion.RESPONSE_JSON
import ai.maum.shinhan_video_processing_server.http.v1.caching.video.SynthesizeHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.server.router

@Configuration
class CachingRouter(
    private val synthesizeHandler: SynthesizeHandler
) {
    @Bean
    fun cachingRoute() = router {
        "/vp/v1/caching".nest {
            "/video".nest {
                "/synthesize".nest {
                    POST("", REQUEST_JSON and RESPONSE_JSON, synthesizeHandler::synthesizeVideo)
                    POST("/streaming", REQUEST_JSON, synthesizeHandler::synthesizeVideoStreaming)
                }
            }
        }
    }
}
