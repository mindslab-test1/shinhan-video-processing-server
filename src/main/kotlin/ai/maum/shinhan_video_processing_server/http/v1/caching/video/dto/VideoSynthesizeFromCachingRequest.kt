package ai.maum.shinhan_video_processing_server.http.v1.caching.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class VideoSynthesizeFromCachingRequest(
    var name: String,
    var background: Background,
    var utter: String,
    var resolution: Resolution,
    var gesture: String,
    var engine: Engine,
    var liveYn: String
) {
    @Serializable
    data class Background(
        var path: String
    )
    @Serializable
    data class Resolution(
        var width: Int,
        var height: Int
    )
    @Serializable
    data class Engine(
        var ttsHost: String,
        var ttsPort: Int,
        var ttsPath: String,
        var w2lHost: String,
        var w2lPort: Int,
        var w2lPath: String
    )
}
