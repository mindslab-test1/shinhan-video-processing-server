package ai.maum.shinhan_video_processing_server.http.v1.caching.video.dto

import kotlinx.serialization.Serializable

@Serializable
data class VideoSynthesizeReportToCachingRequest(
    var name: String,
    var liveYn: String
)
