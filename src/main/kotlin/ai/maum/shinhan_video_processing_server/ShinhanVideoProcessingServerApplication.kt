package ai.maum.shinhan_video_processing_server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShinhanVideoProcessingServerApplication

fun main(args: Array<String>) {
    runApplication<ShinhanVideoProcessingServerApplication>(*args)
}
