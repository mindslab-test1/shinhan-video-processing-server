FROM openjdk:16-jdk-alpine
ARG JAR_FILE=build/libs/shinhan-video-processing-server-0.0.1-SNAPSHOT.jar
ARG ACTIVE_PROFILE=shinhandev
ENV ACTIVE_PROFILE=$ACTIVE_PROFILE
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=${ACTIVE_PROFILE}", "/app.jar"]