import com.google.protobuf.gradle.*
import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jmailen.gradle.kotlinter.tasks.LintTask

group = "ai.maum"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

// gRPC dependencies
val grpcVersion = "1.37.0"
val grpcKotlinVersion = "1.2.0"
val protobufVersion = "3.19.0"

// gRPC directory configurations
val grpcGeneratedDir = ".grpc_generated"
val grpcOutputSubDir = "grpc"
val grpcKtOutputSubDir = "grpckt"

plugins {
    id("org.springframework.boot") version "2.5.4"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("org.jmailen.kotlinter") version "3.4.4"
    id("io.gitlab.arturbosch.detekt") version "1.16.0"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.5.0"
    id("org.jetbrains.dokka") version "1.4.32"
    id("com.google.protobuf") version "0.8.17"
    kotlin("jvm") version "1.5.30"
    kotlin("plugin.spring") version "1.5.30"
    kotlin("plugin.serialization") version "1.5.20"
}

repositories {
    mavenCentral()
    maven { url = uri("https://maven.pkg.jetbrains.space/public/p/kotlinx-html/maven") }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.2.2")

    implementation("io.grpc:grpc-kotlin-stub:$grpcKotlinVersion")
    implementation("io.grpc:grpc-protobuf:$grpcVersion")
    implementation("io.grpc:grpc-netty:$grpcVersion")
    implementation("com.google.protobuf:protobuf-kotlin:$protobufVersion")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")
}

sourceSets {
    main {
        java {
            srcDir("$grpcGeneratedDir/main/$grpcOutputSubDir")
            srcDir("$grpcGeneratedDir/main/java")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:$protobufVersion"
    }

    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:$grpcKotlinVersion:jdk7@jar"
        }
    }

    generateProtoTasks {
        ofSourceSet("main").forEach { task ->
            task.builtins {
                remove("java")
            }
            task.plugins {
                id("grpc") {
                    outputSubDir = grpcOutputSubDir
                }
                id("grpckt") {
                    outputSubDir = grpcKtOutputSubDir
                }
                id("java") {
                    outputSubDir = grpcOutputSubDir
                }
            }
        }
    }
    
    generatedFilesBaseDir = "$projectDir/$grpcGeneratedDir"
}

allOpen {
    annotation("ai.maum.shinhan_video_processing_server.AllOpen")
}

kotlinter {
    ignoreFailures = false
    indentSize = 4
    reporters = arrayOf("checkstyle", "plain")
    experimentalRules = false
    disabledRules = arrayOf("no-wildcard-imports")
}

detekt {
    buildUponDefaultConfig = true
    allRules = false
    config = files("$projectDir/src/main/resources/detekt/default-detekt-config.yml")
    baseline = file("$projectDir/src/main/resources/detekt/baseline.xml")

    reports {
        html.enabled = true
        xml.enabled = true
        txt.enabled = true
        sarif.enabled = true
    }
}

tasks.clean {
    delete("$projectDir/$grpcGeneratedDir")
}

tasks {
    "lintKotlinMain"(LintTask::class) {
        exclude("**/maum/common/**")
        exclude("**/maum/brain/**")
    }
}

tasks.withType<Detekt> {
    // onlyIf { project.hasProperty("runDetekt") }
    jvmTarget = "16"
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
